%global commit c2f5d130c91b1696385a6ae0b5bcfd5214bcc9ca
%global shortcommit %(c=%{commit}; echo ${c:0:7})
Name:                tre
Version:             0.8.0
Release:             5
License:             BSD-2-Clause
Source0:             https://github.com/laurikari/tre/archive/%{commit}/tre-%{commit}.tar.gz
Patch0:              %{name}-chicken.patch
Patch1:              %{name}-tests.patch
Patch2:              %{name}-ldflags.patch
Patch3:              %{name}-CVE-2016-8859.patch
Patch4:              %{name}-issue37.patch
Patch5:              %{name}-issue50.patch
Patch10:             %{name}-python3.patch
Patch11:             Remove-broken-agrep-test-entry.patch

Summary:             POSIX compatible regexp library with approximate matching
URL:                 http://laurikari.net/tre/
BuildRequires:       gettext-devel glibc-langpack-en libtool python3-devel glibc-all-langpacks
Requires:            %{name}-common = %{version}-%{release}
%description
TRE is a lightweight, robust, and efficient POSIX compatible regexp
matching library with some exciting features such as approximate
matching.

%package common
Summary:             Cross-platform files for use with the tre package
BuildArch:           noarch
%description common
This package contains platform-agnostic files used by the TRE
library.

%package devel
Requires:            tre = %{version}-%{release}
Summary:             Development files for use with the tre package
%description devel
This package contains header files and static libraries for use when
building applications which use the TRE library.

%package -n python3-%{name}
Summary:             Python bindings for the tre library
%{?python_provide:%python_provide python3-tre}
%description -n python3-%{name}
This package contains the python bindings for the TRE library.

%package -n agrep
Summary:             Approximate grep utility
%description -n agrep
The agrep tool is similar to the commonly used grep utility, but agrep
can be used to search for approximate matches.
The agrep tool searches text input for lines (or records separated by
strings matching arbitrary regexps) that contain an approximate, or
fuzzy, match to a specified regexp, and prints the matching lines.
Limits can be set on how many errors of each kind are allowed, or
only the best matching lines can be output.
Unlike other agrep implementations, TRE agrep allows full POSIX
regexps of any length, any number of errors, and non-uniform costs.

%prep
%setup -q -n tre-%{commit}
%patch0 -p1 -b .chicken
%patch1 -p1 -b .tests
%patch2 -p1 -b .ldflags
%patch3 -p1 -b .CVE-2016-8859
%patch4 -p1 -b .issue37
%patch5 -p1 -b .issue50
%patch10 -p1
%patch11 -p1
touch ChangeLog
autoreconf -vif

%build
%configure --disable-static --disable-rpath
%{__make} %{?_smp_mflags}
pushd python
%py3_build
popd

%install
%{__make} install DESTDIR=$RPM_BUILD_ROOT
pushd python
%py3_install
popd
rm $RPM_BUILD_ROOT%{_libdir}/*.la
%find_lang %{name}

%check
%{__make} check
%ldconfig_scriptlets

%files
%{_libdir}/libtre.so.*

%files common -f %{name}.lang
%doc AUTHORS ChangeLog LICENSE NEWS README THANKS TODO
%doc doc/*.html doc/*.css

%files devel
%{_libdir}/libtre.so
%{_libdir}/pkgconfig/*
%{_includedir}/*

%files -n python3-%{name}
%attr(0755,root,root) %{python3_sitearch}/tre.cpython-%{python3_version_nodots}*-%{_arch}-linux%{_gnu}.so
%{python3_sitearch}/tre-%{version}-py%{python3_version}.egg-info

%files -n agrep
%{_bindir}/agrep
%{_mandir}/man1/agrep.1*

%changelog
* Thu Jun 06 2024 Wenlong Zhang <zhangwenlong@loongson.cn> - 0.8.0-5
- fix build error for loongarch64

* Sat Jul 22 2023 xu_ping <707078654@qq.com> - 0.8.0-4
- Fix test failure

* Mon Nov 14 2022 huajingyun <huajingyun@loongson.cn> - 0.8.0-3
- Fix build error on loongarch

* Tue May 10 2022 caodongxia <caodongxia@h-partners.com> - 0.8.0-2
- License compliance rectification

* Wed May 12 2021 zhangtao <zhangtao307@huawei.com> - 0.8.0-1
- package init
